import java.util.LinkedList;

aspect CheckFinalVariables {
	public void CompilationUnit.checkFinalVariables() {
		System.out.println("checking " + pathName());
		for (Variable var: effectivelyFinalVariables()) {
			ASTNode node = (ASTNode) var;
			System.out.print(node.sourceFile() + ":" + node.location() + ": ");
			if (var.isParameter()) {
				System.out.print("parameter ");
			} else {
				System.out.print("variable ");
			}
			System.out.print(var.name());
			System.out.println(" is effectively final");
		}
	}

	coll java.util.List<Variable> CompilationUnit.effectivelyFinalVariables()
		[new LinkedList<Variable>()]
		with add
		root CompilationUnit;

	inh CompilationUnit VariableDecl.compilationUnit();

	VariableDecl contributes declaration()
		when declaration().isEffectivelyFinal()
		to CompilationUnit.effectivelyFinalVariables()
		for compilationUnit();

	inh CompilationUnit ParameterDeclaration.compilationUnit();

	ParameterDeclaration contributes this
		when isEffectivelyFinal()
		to CompilationUnit.effectivelyFinalVariables()
		for compilationUnit();

	inh VarDeclStmt VariableDecl.declStmt();
	eq VarDeclStmt.getChild().declStmt() = this;
	eq FieldDecl.getChild().declStmt() { throw new Error("no enclosing var decl stmt"); }

	inh int VariableDecl.declIndex();
	eq VarDeclStmt.getVariableDecl(int i).declIndex() = i;
	eq FieldDecl.getChild().declIndex() { throw new Error("no enclosing var decl stmt"); }

	syn VariableDeclaration VariableDecl.declaration() =
		declStmt().getSingleDecl(declIndex());

}
